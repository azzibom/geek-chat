package by.azzibom.chat.network;

import java.time.LocalDateTime;
import java.util.Objects;

public class Message {

    private LocalDateTime time = LocalDateTime.now();
    private String name;
    private String value;

    public Message(String name, String text) {
        this.name = name;
        this.value = text;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(time, message.time) &&
                Objects.equals(name, message.name) &&
                Objects.equals(value, message.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, name, value);
    }

    @Override
    public String toString() {
        return String.format("[%s] <%s>: %s", time, name, value);
    }
}
