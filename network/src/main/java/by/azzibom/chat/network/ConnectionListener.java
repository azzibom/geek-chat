package by.azzibom.chat.network;

public interface ConnectionListener {

    void onConnectionReady(Connection connection);

    void onReceiveMessage(Connection connection, Message message);

    void onDisconnect(Connection connection);

    void onException(Connection connection, Exception e);
}
