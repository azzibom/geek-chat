package by.azzibom.chat.network;

public interface Connection {

    void sendMessage(Message message);

    void disconnect();
}
