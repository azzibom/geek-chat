package by.azzibom.chat.network;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class TCPConnection implements Connection, Runnable {

    private final Socket socket;
    private final Thread rsThread;
    private final ConnectionListener eventListener;

    private final BufferedWriter bw;
    private final BufferedReader br;

    private final Gson gson;

    public TCPConnection(ConnectionListener eventListener, String ip, int port) throws IOException {
        this(new Socket(ip, port), eventListener);
    }

    public TCPConnection(Socket socket, final ConnectionListener eventListener) throws IOException {
        this.socket = socket;
        this.eventListener = eventListener;
        this.gson = new Gson();
        this.bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        this.br = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
        this.rsThread = new Thread(this);
        rsThread.start();
    }

    @Override
    public synchronized void sendMessage(Message message) {
        try {
            String jsonMessage = gson.toJson(message);
            bw.append(jsonMessage)
                    .append("\r\n")
                    .flush();
        } catch (IOException e) {
            eventListener.onException(this, e);
            disconnect();
        }
    }

    @Override
    public synchronized void disconnect() {
        rsThread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onException(this, e);
        }
    }

    @Override
    public void run() {
        try {
            eventListener.onConnectionReady(TCPConnection.this);
            while (!rsThread.isInterrupted()) {
                String str;
                if ((str = br.readLine()) != null) {
                    Message message = gson.fromJson(str, Message.class);
                    eventListener.onReceiveMessage(TCPConnection.this, message);
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
        } finally {
            eventListener.onDisconnect(TCPConnection.this);
        }
    }

    @Override
    public String toString() {
        return "TCPConnection: " + socket.getInetAddress() + ":" + socket.getPort();
    }
}
