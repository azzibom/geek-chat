package by.azzibom.chat.server;

import by.azzibom.chat.network.Connection;
import by.azzibom.chat.network.ConnectionListener;
import by.azzibom.chat.network.Message;

import java.util.ArrayList;
import java.util.List;

public class ServerConnectionListener implements ConnectionListener {

    private List<Connection> connections = new ArrayList<>();
    private String name = "SERVER";

    @Override
    public synchronized void onConnectionReady(Connection connection) {
        connections.add(connection);
        sendToAllConnections(new Message(name, "Client connected: " + connection));
    }

    @Override
    public synchronized void onReceiveMessage(Connection connection, Message message) {
        sendToAllConnections(message);
    }

    @Override
    public synchronized void onDisconnect(Connection connection) {
        connections.remove(connection);
        Message message = new Message(name, "Client disconnected:" + connection);
        sendToAllConnections(message);
    }

    @Override
    public synchronized void onException(Connection connection, Exception e) {
        System.out.println("TCPConnection exception: " + e);
    }

    private void sendToAllConnections(Message message) {
        System.out.println(message);
        for (Connection connection : connections) {
            connection.sendMessage(message);
        }
    }
}
