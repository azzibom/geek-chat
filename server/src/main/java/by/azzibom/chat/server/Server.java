package by.azzibom.chat.server;

import by.azzibom.chat.network.ConnectionListener;
import by.azzibom.chat.network.TCPConnection;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {

    private static final int DEFAULT_PORT = 8189;

    private final int port;

    private Server() {
        this(DEFAULT_PORT);
    }

    private Server(int port) {
        this.port = port;
    }

    private void connect() {
        try (ServerSocket ss = new ServerSocket(port)) {
            System.out.println(String.format("Server running on port %d...", port));
            ConnectionListener listener = new ServerConnectionListener();
            while (true) {
                try {
                    new TCPConnection(ss.accept(), listener);
                } catch (IOException e) {
                    System.out.println("TCPConnection exception: " + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        Server server;
        if (args != null && args.length > 0) {
            int port = Integer.parseInt(args[0]);
            server = new Server(port);
        } else {
            server = new Server();
        }
        server.connect();
    }
}
