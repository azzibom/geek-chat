package by.azzibom.chat.client;

import by.azzibom.chat.network.Connection;
import by.azzibom.chat.network.ConnectionListener;
import by.azzibom.chat.network.Message;
import by.azzibom.chat.network.TCPConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class ClientWindow extends JFrame {

    private static final String IP = "localhost";
    private static final int PORT = 8189;

    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    private Connection connection;

    private MessageListModelAdapter messageListModelAdapter = new MessageListModelAdapter();
    private JTextField inputField;
    private JTextField nickField;

    private ClientWindow() {
        super("tcp chat");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setAlwaysOnTop(true);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                connection.disconnect();
            }
        });
        JList<Message> messageList = new JList<>(messageListModelAdapter);
        messageList.setCellRenderer(new ListCellRenderer<Message>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends Message> list, Message value, int index, boolean isSelected, boolean cellHasFocus) {
                boolean my = nickField.getText().equals(value.getName());
                JPanel root = new JPanel(new FlowLayout(my ? FlowLayout.RIGHT : FlowLayout.LEFT));
                root.setOpaque(false);
                GridBagLayout gbl = new GridBagLayout();
                JPanel messagePanel = new JPanel(gbl);
                messagePanel.setOpaque(false);
                messagePanel.setBorder(BorderFactory.createEtchedBorder());

                GridBagConstraints gbc = new GridBagConstraints();
                gbc.gridx = 0;
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.gridy = 0;

                JLabel timeLabel = new JLabel(value.getTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                timeLabel.setEnabled(false);
                messagePanel.add(timeLabel, gbc);

                if (!my) {
                    gbc.gridy++;
                    JLabel senderLabel = new JLabel(value.getName());
                    messagePanel.add(senderLabel, gbc);
                }

                gbc.gridy++;
                JLabel valueLabel = new JLabel(value.getValue());
                messagePanel.add(valueLabel, gbc);

                root.add(messagePanel);
                return root;
            }
        });
        nickField = new JTextField("client", 10);
        JPanel inputPanel = new JPanel(new BorderLayout());
        inputField = new JTextField();
        JButton sendButton = new JButton("send");

        inputField.addActionListener(this::send);
        sendButton.addActionListener(this::send);

        JScrollPane scrollPane = new JScrollPane(messageList);
        add(scrollPane, BorderLayout.CENTER);
        add(inputPanel, BorderLayout.SOUTH);
        inputPanel.add(nickField, BorderLayout.WEST);
        inputPanel.add(inputField, BorderLayout.CENTER);
        inputPanel.add(sendButton, BorderLayout.EAST);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        ConnectionListener listener = new ClientConnectionListener(this);
        try {
            connection = new TCPConnection(listener, IP, PORT);
        } catch (IOException e) {
            listener.onException(connection, e);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ClientWindow::new);
    }

    private void send(ActionEvent e) {
        String nickname = nickField.getText();
        String message = inputField.getText();
        if (message.equals("")) return;
        inputField.setText(null);
        connection.sendMessage(new Message(nickname, message));
    }

    synchronized void printMessage(Message message) {
        SwingUtilities.invokeLater(() -> messageListModelAdapter.add(message));
    }
}
