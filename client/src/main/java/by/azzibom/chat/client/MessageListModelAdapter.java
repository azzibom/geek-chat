package by.azzibom.chat.client;

import by.azzibom.chat.network.Message;

import javax.swing.*;
import java.util.*;
import java.util.function.UnaryOperator;

/**
 * @author Ihar Misevich
 */
public class MessageListModelAdapter extends AbstractListModel<Message> implements List<Message> {

    private List<Message> list = new ArrayList<>();

    @Override
    public int getSize() {
        return size();
    }

    @Override
    public Message getElementAt(int index) {
        return get(index);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<Message> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(Message message) {
        boolean res = list.add(message);
        fireIntervalAdded(this, list.size(), list.size());
        return res;
    }

    @Override
    public boolean remove(Object o) {
        int index = list.indexOf(o);
        boolean res = list.remove(o);
        if (res) {
            fireIntervalRemoved(this, index, index);
        }
        return res;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Message> c) {
        int a = list.size();
        boolean res = list.addAll(c);
        if (res) {
            fireIntervalAdded(this, a, list.size());
        }
        return res;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Message> c) {
        boolean res = list.addAll(index, c);
        if (res) {
            fireIntervalAdded(this, index, index + c.size());
        }
        return res;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean res = list.removeAll(c);
        if (res) {
            fireContentsChanged(this, 0, size());
        }
        return res;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void replaceAll(UnaryOperator<Message> operator) {
        list.replaceAll(operator);
    }

    @Override
    public void sort(Comparator<? super Message> c) {
        list.sort(c);
    }

    @Override
    public void clear() {
        int size = size();
        list.clear();
        fireIntervalRemoved(this, 0, size);
    }

    @Override
    public boolean equals(Object o) {
        return list.equals(o);
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }

    @Override
    public Message get(int index) {
        return list.get(index);
    }

    @Override
    public Message set(int index, Message element) {
        try {
            return list.set(index, element);
        } finally {
            fireContentsChanged(this, index, index);
        }
    }

    @Override
    public void add(int index, Message element) {
        try {
            list.add(index, element);
        } finally {
            fireIntervalAdded(this, index, index);
        }
    }

    @Override
    public Message remove(int index) {
        try {
            return list.remove(index);
        } finally {
            fireIntervalRemoved(this, index, index);
        }
    }

    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @Override
    public ListIterator<Message> listIterator() {
        return list.listIterator();
    }

    @Override
    public ListIterator<Message> listIterator(int index) {
        return list.listIterator(index);
    }

    @Override
    public List<Message> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }
}
