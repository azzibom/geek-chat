package by.azzibom.chat.client;

import by.azzibom.chat.network.Connection;
import by.azzibom.chat.network.ConnectionListener;
import by.azzibom.chat.network.Message;

public class ClientConnectionListener implements ConnectionListener {

    private ClientWindow clientWindow;
    private String systemName = "SYSTEM";

    ClientConnectionListener(ClientWindow clientWindow) {
        this.clientWindow = clientWindow;
    }

    @Override
    public void onConnectionReady(Connection connection) {
        clientWindow.printMessage(new Message(systemName, "Connection ready..."));
    }

    @Override
    public void onReceiveMessage(Connection connection, Message message) {
        clientWindow.printMessage(message);
    }

    @Override
    public void onDisconnect(Connection connection) {
        clientWindow.printMessage(new Message(systemName, "Connection close"));
    }

    @Override
    public void onException(Connection connection, Exception e) {
        clientWindow.printMessage(new Message(systemName, "Connection exception: " + e));
    }
}
